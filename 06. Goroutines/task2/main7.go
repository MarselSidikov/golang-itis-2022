package main

import (
	"06._Goroutines/generator"
	"fmt"
	"os"
	"runtime/trace"
	"sync"
	"time"
)

const TextSize = 100_000_000
const GoroutinesCount = 10
const Range = TextSize / GoroutinesCount

type Counter struct {
	counts map[rune]int
	mutex  sync.Mutex
}

func (c *Counter) Count(char rune) {
	c.mutex.Lock()
	c.counts[char]++
	c.mutex.Unlock()
}

func RunProcess(c *Counter, text []rune, from, to int, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := from; i <= to; i++ {
		c.Count(text[i])
	}
}

func (c *Counter) Print() {
	for char, count := range c.counts {
		fmt.Printf("%c -> %d \n", char, count)
	}
}

func main() {

	f, err := os.Create("trace1.out")
	if err != nil {
		panic(err)
	}

	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()

	var wg sync.WaitGroup
	wg.Add(GoroutinesCount)

	characters := []rune(generator.RandomString(TextSize))

	c := &Counter{counts: make(map[rune]int)}

	start := time.Now()

	for i := 0; i < TextSize; i += Range {
		go RunProcess(c, characters, i, i+(Range-1), &wg)
	}

	wg.Wait()

	fmt.Println(time.Now().Sub(start).Seconds())
	c.Print()
}
