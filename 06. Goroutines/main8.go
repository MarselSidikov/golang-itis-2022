package main

import (
	"06._Goroutines/generator"
	"fmt"
	"os"
	"runtime/trace"
	"sync"
	"sync/atomic"
	"time"
)

const TextSize = 100_000_000
const GoroutinesCount = 4
const Range = TextSize / GoroutinesCount

type Counter struct {
	counts map[rune]*int32
	mutex  sync.RWMutex
}

func (c *Counter) Count(char rune) {
	var count *int32

	if count = c.GetCount(char); count == nil {
		count = c.InitCount(char)
	}

	atomic.AddInt32(count, 1)
}

func (c *Counter) GetCount(character rune) *int32 {
	defer c.mutex.RUnlock()
	c.mutex.RLock()
	return c.counts[character]
}

func (c *Counter) InitCount(character rune) *int32 {
	count := c.GetCount(character)

	if count == nil {
		defer c.mutex.Unlock()
		c.mutex.Lock()
		value := int32(0)
		count = &value
		c.counts[character] = count
	}

	return count
}

func RunProcess(c *Counter, text []rune, from, to int, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := from; i <= to; i++ {
		c.Count(text[i])
	}
}

func (c *Counter) Print() {
	for char, count := range c.counts {
		fmt.Printf("%c -> %d \n", char, *count)
	}
}

func main() {

	f, err := os.Create("trace2.out")
	if err != nil {
		panic(err)
	}

	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()

	var wg sync.WaitGroup
	wg.Add(GoroutinesCount)

	characters := []rune(generator.RandomString(TextSize))

	c := &Counter{counts: make(map[rune]*int32)}

	start := time.Now()

	for i := 0; i < TextSize; i += Range {
		go RunProcess(c, characters, i, i+(Range-1), &wg)
	}

	wg.Wait()

	fmt.Println(time.Now().Sub(start).Seconds())
	c.Print()
}
