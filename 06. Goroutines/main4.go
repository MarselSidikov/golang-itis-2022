package main

import "fmt"

func Example(from, to int) (sum int) {

	defer func() {
		fmt.Println("First Defer")
		sum = 100
	}()

	defer func() {
		fmt.Println("Last Defer")
		sum = 200
	}()

	for i := from; i <= to; i++ {
		sum += i
	}

	return
}

func PrintValue(value int) {
	fmt.Println(value)
}

func Example2(from, to int) (sum int) {
	for i := from; i <= to; i++ {
		sum += i

		// func -> 3
		// func -> 3
		// func -> 3

		//defer func() {
		//	fmt.Println(i)
		//}()

		//defer PrintValue(i)

		//defer func(argument int) {
		//	fmt.Println(argument)
		//}(i)

		value := i
		defer func() {
			fmt.Println(value)
		}()
		
	}

	return
}

func main() {
	result := Example2(1, 5)
	fmt.Printf("%d", result)
}
