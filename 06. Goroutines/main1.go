package main

import (
	"fmt"
	"sync"
)

var i = 0

func main() {

	var wg sync.WaitGroup

	var mutex sync.Mutex
	printFunction := func() {
		mutex.Lock()
		fmt.Printf("До %v: \n", i)
		i++
		fmt.Printf("После %v: \n", i)
		mutex.Unlock()
		defer wg.Done()
	}

	wg.Add(3)

	go printFunction()
	go printFunction()
	go printFunction()

	wg.Wait()

	fmt.Println(i)
}
