package main

import (
	"fmt"
	"time"
)

var variable = 0

func main() {

	printFunction := func(c chan int) {
		fmt.Printf("Начинаем чтение из канала ... \n")
		data := <-c
		fmt.Printf("Значение %v прочитано, увеличиваем \n", data)
		data++
		fmt.Printf("Записываем %v в канал \n", data)
		c <- data
		fmt.Printf("Значение %v записано \n", data)
	}

	var c = make(chan int)

	go printFunction(c)
	go printFunction(c)
	go printFunction(c)

	time.Sleep(5 * time.Second)
	c <- variable

	data := <-c
	fmt.Println(data)
}
