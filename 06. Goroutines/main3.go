package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func PartSum(from, to int, s []int, c chan<- int, group *sync.WaitGroup) {
	defer group.Done()
	fmt.Printf("Запустили с %v по %v \n", from, to)
	sum := 0
	for i := from; i <= to; i++ {
		sum += s[i]
	}
	fmt.Printf("Записали %v \n", sum)
	c <- sum
}

func SumOfArray(c <-chan int, result chan<- int) {
	sum := 0
	for data := range c {
		fmt.Printf("Прочитали %v \n", data)
		sum += data
	}

	fmt.Printf("Отправляем результат %v \n", sum)
	result <- sum
}

func main() {
	rand.Seed(time.Now().Unix())
	var wg sync.WaitGroup
	wg.Add(3)

	//s := []int{7, 2, 8, -9, 4, 0}

	s := rand.Perm(9000)

	c := make(chan int)
	result := make(chan int)

	go PartSum(0, 2999, s, c, &wg)
	go PartSum(3000, 5999, s, c, &wg)
	go PartSum(6000, 8999, s, c, &wg)
	go SumOfArray(c, result)

	wg.Wait()
	close(c)
	value := <-result

	fmt.Printf("Общий результат %v", value)

}
