package main

import (
	"06._Goroutines/generator"
	"fmt"
	"time"
)

const TextSize = 100_000_000

type Counter struct {
	counts map[rune]int
}

func (c *Counter) Count(char rune) {
	c.counts[char]++
}

func (c *Counter) Print() {
	for char, count := range c.counts {
		fmt.Printf("%c -> %d \n", char, count)
	}
}

func main() {
	randomString := generator.RandomString(TextSize)

	c := &Counter{counts: make(map[rune]int)}

	start := time.Now()
	for _, char := range randomString {
		c.Count(char)
	}
	fmt.Println(time.Now().Sub(start).Seconds())
	c.Print()
}
