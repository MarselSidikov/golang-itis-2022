package main

import "fmt"

type Number struct {
	value int
}

func (n Number) Print() {
	fmt.Printf("%d \n", n.value)
}

func (n *Number) PrintByPointer() {
	fmt.Printf("%d \n", n.value)
}

func (n *Number) Increment() {
	n.value++
}

func main() {
	var n Number

	n.value = 0

	defer n.Print() // кладем в стек копию исходной структуры, следовательно copy number -> value 0
	n.Increment()   // увеличиваем исходный value, следовательно number -> value 1

	defer n.Print() // кладем в стек копию исходной структуры, следовательно copy number -> value 1
	n.Increment()   // // увеличиваем исходный value, следовательно number -> value 1

	defer n.PrintByPointer() // кладем в стек исходную структуру, следовательно copy number -> 4
	n.Increment()            // увеличиваем исходный value, следовательно number -> value 3

	defer n.PrintByPointer() // кладем в стек исходную структуру, следовательно copy number -> 4
	n.Increment()            // увеличиваем исходный value, следовательно number -> value 4

	// 4, 4, 1, 0
}
