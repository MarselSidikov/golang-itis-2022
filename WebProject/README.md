1. Обработка ошибок
2. Context-ы (Fiber и контекст приложения)
3. Валидация
4. Запуск/остановка сервера
5. DTO
6. Сборка Go-проекта
7. Более полная документация Swagger

```
go get -u github.com/gofiber/swagger
```

```
swag init -g ./cmd/api/main.go
```