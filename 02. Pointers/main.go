package main

import (
	"fmt"
	"unsafe"
)

func swap(a, b *int) {
	temp := *a
	*a = *b
	*b = temp
}

func main() {

	x, y := 10, 5
	fmt.Printf("x = %v, y = %v \n", x, y)
	swap(&x, &y)
	fmt.Printf("x = %v, y = %v \n", x, y)

	var number int32
	var number2 int32
	number = 5

	fmt.Printf("%v \n", &number)
	fmt.Printf("%v \n", &number2)
	fmt.Printf("%v \n", unsafe.Sizeof(number))

	var pointer *int32
	fmt.Printf("%v \n", unsafe.Sizeof(pointer))

	pointer = &number
	fmt.Printf("%v \n", pointer)

	pointerValueByAddress := *pointer
	fmt.Printf("%v \n", pointerValueByAddress)

	//var anotherPointer *float32
	anotherPointer := new(float32)
	value := *anotherPointer

	fmt.Printf("%v", value)

}
