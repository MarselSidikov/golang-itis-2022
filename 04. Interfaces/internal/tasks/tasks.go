package tasks

import "fmt"

type State int8

const (
	PREPARED State = 1
	DONE     State = 2
)

type Task interface {
	Execute()
	ChangeState(state State)
}

type NumbersPrinter struct {
	State State
}

func New() *NumbersPrinter {
	return &NumbersPrinter{State: PREPARED}
}

func (numbersPrinter *NumbersPrinter) Execute() {
	fmt.Printf("State = %v and number = 1, 2, 3 \n", numbersPrinter.State)
}

func (numbersPrinter *NumbersPrinter) ChangeState(state State) {
	numbersPrinter.State = state
}
