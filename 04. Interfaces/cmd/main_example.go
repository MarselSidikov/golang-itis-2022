package main

import (
	"04._Interfaces/internal/tasks"
	"fmt"
)

func main_0() {
	//numbersPrinter := tasks.New()
	//fmt.Printf("%v", numbersPrinter)

	// Human human; // human == null
	var numbersPrinter tasks.NumbersPrinter

	fmt.Printf("%v \n", numbersPrinter)

	var task tasks.Task
	//task = numbersPrinter

	fmt.Printf("%T %v \n", task, task)

	if task == nil {
		fmt.Println("task is nil")
	}
}
