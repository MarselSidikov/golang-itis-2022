package main

import (
	"04._Interfaces/internal/tasks"
	"fmt"
)

func ProcessTask(task tasks.Task) {
	task.Execute()
	task.ChangeState(tasks.DONE)
}

func main() {
	var numbersPinter = &tasks.NumbersPrinter{State: tasks.PREPARED}

	var task tasks.Task

	task = numbersPinter

	task.Execute()

	task.ChangeState(tasks.DONE)

	fmt.Printf("%v", numbersPinter.State)

}
