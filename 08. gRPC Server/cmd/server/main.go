package main

import (
	"08._gRPC_Server/internal/config"
	"08._gRPC_Server/internal/repository/mongo"
	"08._gRPC_Server/internal/server"
	pb "08._gRPC_Server/proto"
	"context"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
)

var (
	host = "localhost"
	port = "5000"
)

func main() {
	ctx := context.Background()

	err := setupViper()

	if err != nil {
		log.Fatalf("error reading yml file: %v", err)
	}

	addr := fmt.Sprintf("%s:%s", host, port)
	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("error starting tcp listener: %v", err)
	}

	mongoDataBase, err := config.SetupMongoDataBase(ctx)

	if err != nil {
		log.Fatalf("error starting mongo : %v", err)
	}

	userRepository := mongo.NewUserRepository(mongoDataBase.Collection("users"))

	userServer := server.NewUserServer(userRepository)

	grpcServer := grpc.NewServer()

	pb.RegisterUserServiceServer(grpcServer, userServer)

	log.Printf("gRPC started at %v\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("error starting gRPC : %v", err)
	}

}

func setupViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
