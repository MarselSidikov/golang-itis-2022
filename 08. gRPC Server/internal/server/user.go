package server

import (
	"08._gRPC_Server/internal/core"
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)
import pb "08._gRPC_Server/proto"

type UserRepository interface {
	GetById(ctx context.Context, id string) (*core.User, error)
}

type UserServer struct {
	pb.UserServiceServer
	userRepository UserRepository
}

func NewUserServer(repository UserRepository) *UserServer {
	return &UserServer{
		userRepository: repository,
	}
}

func (server *UserServer) GetUser(ctx context.Context, request *pb.UserRequest) (response *pb.UserResponse, err error) {
	user, err := server.userRepository.GetById(ctx, request.GetId())

	if err != nil {
		return nil, err
	}

	if user == nil {
		user = &core.User{
			ID:        primitive.ObjectID{},
			FirstName: "",
			LastName:  "",
		}
	}

	return &pb.UserResponse{Name: user.FirstName}, nil
}
