package main

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"
)

type Processor = func(ctx context.Context) (string, error)

func doWork(workType string, durationSecond int, result chan<- string) {
	fmt.Println("Work with - " + workType)
	time.Sleep(time.Duration(durationSecond) * time.Second)
	result <- "Result from " + workType
}

func goToRemoteServer(ctx context.Context) (result string, err error) {
	//
	//if ctx.Err() == context.Canceled {
	//	fmt.Println("RemoteServer cancelled")
	//	return "", nil
	//}
	//
	//fmt.Println("RemoteServer is working...")
	//resultChannel := make(chan string)
	//go doWork("Remote Server", 2, resultChannel)
	//
	//for {
	//	select {
	//	case <-ctx.Done():
	//		if ctx.Err() == context.Canceled {
	//			fmt.Println("RemoteServer cancelled")
	//			return "", nil
	//		}
	//	case result = <-resultChannel:
	//		fmt.Println("RemoteServer done")
	//		return result, nil
	//	}
	//}
	time.Sleep(2 * time.Second)
	return "", errors.New("REMOTE SERVER NOT WORKING")
}

func goToDataBase(ctx context.Context) (result string, err error) {

	if ctx.Err() == context.Canceled {
		fmt.Println("DataBase cancelled")
		return "", nil
	}

	fmt.Println("DataBase is working...")

	resultChannel := make(chan string)
	go doWork("DataBase", 4, resultChannel)

	for {
		select {
		case <-ctx.Done():
			if ctx.Err() == context.Canceled {
				fmt.Println("DataBase cancelled")
				return "", nil
			}
		case result = <-resultChannel:
			fmt.Println("DataBase done")
			return result, nil
		}
	}
}

func goToDisk(ctx context.Context) (result string, err error) {

	if ctx.Err() == context.Canceled {
		fmt.Println("Disk cancelled")
		return "", nil
	}

	fmt.Println("Disk is working...")

	resultChannel := make(chan string)
	go doWork("Disk", 8, resultChannel)

	for {
		select {
		case <-ctx.Done():
			if ctx.Err() == context.Canceled {
				fmt.Println("Disk cancelled")
				return "", nil
			}
		case result = <-resultChannel:
			fmt.Println("Disk done")
			return result, nil
		}
	}
}

func processRequest(ctx context.Context, processors []Processor) {
	ctxWithCancel, cancel := context.WithCancel(ctx)
	defer cancel()

	var wg = &sync.WaitGroup{}
	wg.Add(len(processors))

	for _, processor := range processors {
		processor := processor
		go func() {
			defer wg.Done()
			result, err := processor(ctxWithCancel)

			if err != nil {
				cancel()
			} else if result != "" {
				fmt.Println("-> " + result)
			}
		}()
	}
	wg.Wait()
}

func main() {
	ctx := context.Background()
	processors := []Processor{goToRemoteServer, goToDataBase, goToDisk}
	processRequest(ctx, processors)
}
