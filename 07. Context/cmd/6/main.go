package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func GetUser(id string) (*User, error) {
	var data UserDataResponse
	response, err := http.Get("http://localhost:3000/users/" + id)

	if err != nil {
		return nil, err
	}

	if bytes, err := io.ReadAll(response.Body); err == nil {
		err = json.Unmarshal(bytes, &data)
	}

	if err != nil {
		return nil, err
	}

	user := data.User
	return &user, nil

}

func main() {
	user, err := GetUser("6353d0a568ba7fec3e14f3ac")
	fmt.Println(user)
	fmt.Println(err)
}
