package main

type UserDataResponse struct {
	User User `json:"user"`
}
type User struct {
	ID        string `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}
