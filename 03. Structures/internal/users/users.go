package users

import "fmt"

const MinPasswordLength = 7

type User struct {
	Email    string
	Password string
	Age      int
}

// user *User - pointer receiver

func (user *User) ChangePassword(newPassword string) {
	if len(newPassword) >= MinPasswordLength {
		user.Password = newPassword // (*user).Password
	}
}

// user User - value receiver

func (user User) PrintInformation() {
	fmt.Printf("Email - %v, age - %v \n", user.Email, user.Age)
}
