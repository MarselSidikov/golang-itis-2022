package main

import (
	"03._Structures/internal/users"
	"fmt"
	"unsafe"
)

func main() {
	var marsel users.User
	marsel.Email = "sidikov.marsel@gmail.com"
	marsel.Password = "qwerty007"
	marsel.Age = 28

	fmt.Printf("Type - %T, value - %v, size - %v \n", marsel, marsel, unsafe.Sizeof(marsel))

	igor := users.User{
		Email:    "igor@gmail.com",
		Password: "qwerty009",
		Age:      30,
	}

	fmt.Printf("Type - %T, value - %v, size - %v \n", igor, igor, unsafe.Sizeof(igor))

	//users.ChangePassword(&igor, "qwerty10")

	maxim := &users.User{
		Email:    "maxim@gmail.com",
		Password: "qwerty10",
		Age:      30,
	}

	//users.ChangePassword(maxim, "qwerty77")

	fmt.Printf("Type - %T, value - %v, size - %v \n", maxim, maxim, unsafe.Sizeof(maxim))

	//users.PrintInformation(*maxim)
	igor.PrintInformation()
	maxim.PrintInformation()

	maxim.ChangePassword("qwerty100")
	igor.ChangePassword("qwerty9999")

	fmt.Printf("Type - %T, value - %v, size - %v \n", igor, igor, unsafe.Sizeof(igor))
	fmt.Printf("Type - %T, value - %v, size - %v \n", maxim, maxim, unsafe.Sizeof(maxim))
}
