package ru.itis.users.service;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.users.pb.UserRequest;
import ru.itis.users.pb.UserServiceGrpc;

@Service
public class UserServiceClient {

    @GrpcClient("user-service")
    private UserServiceGrpc.UserServiceBlockingStub service;

    public String getNameOfUser(String id) {
        return service.getUser(UserRequest.newBuilder()
                        .setId(id)
                .build()).getName();
    }
}
