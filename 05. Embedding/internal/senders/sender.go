package senders

type EmailSender interface {
	SendMail(email, message string)
}

type SmsSender interface {
	SendSms(phone, message string)
}

type Sender interface {
	EmailSender
	SmsSender
}

type CustomSender struct {
	ServiceSmsRuSender
	SmtpSender
}
