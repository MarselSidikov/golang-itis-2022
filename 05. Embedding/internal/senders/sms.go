package senders

import "fmt"

type ServiceSmsRuSender struct {
	Login    string
	Password string
}

func (sender *ServiceSmsRuSender) SendSms(phone, message string) {
	fmt.Printf("Вход в систему Login - %v, Password - %v \n", sender.Login, sender.Password)
	fmt.Printf("Отправлено смс на <%v>: %v \n", phone, message)
}
