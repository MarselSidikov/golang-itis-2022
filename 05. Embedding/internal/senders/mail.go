package senders

import "fmt"

type SmtpSender struct {
	Host     string
	Port     string
	Username string
	Password string
}

func (sender *SmtpSender) SendMail(email, message string) {
	fmt.Printf("SMTP подключение host - %v, port - %v, username - %v, password - %v \n",
		sender.Host,
		sender.Port,
		sender.Username,
		sender.Password)

	fmt.Printf("Отправлено сообщение на <%v>: %v \n", email, message)
}
