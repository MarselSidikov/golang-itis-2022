package main

import "05._Embedding/internal/senders"

func NotifyAboutBlock(sender senders.Sender, email, phone string) {
	sender.SendSms(phone, "Пользователь заблокирован")
	sender.SendMail(email, "Пользователь заблокирован")
}

func NotifyAboutEnter(userId string, sender senders.SmsSender) {
	phone := findPhoneByUserId(userId)
	sender.SendSms(phone, "Был выполнен вход")
}
func NotifyAboutPasswordReset(userId string, sender senders.EmailSender) {
	email := findEmailByUserId(userId)
	sender.SendMail(email, "Был изменен пароль")
}

func findPhoneByUserId(userId string) string {
	return "+78999999999"
}

func findEmailByUserId(userId string) string {
	return "sidikov.marsel@gmail.com"
}

func main() {

	customSender := &senders.CustomSender{
		ServiceSmsRuSender: senders.ServiceSmsRuSender{
			Login:    "marsel",
			Password: "qwerty",
		},
		SmtpSender: senders.SmtpSender{
			Host:     "gmail.com",
			Port:     "587",
			Username: "marsel",
			Password: "qwerty007",
		}}

	NotifyAboutEnter("1", customSender)
	NotifyAboutPasswordReset("1", customSender)

	NotifyAboutBlock(customSender, "sidikov@gmail.com", "+7937292922")
}
